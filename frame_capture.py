import cv2

class FrameCapture(object):
    def __init__(self, device):
        self.video = cv2.VideoCapture(device)
    
    def __del__(self):
        self.video.release()
    
    def capture_frame(self):
        _, frame = self.video.read()

        return frame