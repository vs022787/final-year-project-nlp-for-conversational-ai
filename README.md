# Final Year Project - NLP for Conversational AI
Final Year Project - NLP for Conversational AI

### Setup
Install packages specified in the "requirements.txt" file into environment.

Additional optional step:
Manually copy Object Detection folder from tensorflow github repo and install by performing build and install procedure on "setup.py" file located in "..\models\research" directory


## Project Description
This project has formulated a working solution that implemented Sign Language feature into a Conversational system in effort to allow communication not only through speech and text but through gestures/sign language with the addition of Sentimental Analysis to provide detection of positive/negative expression from text between both entities throughout the conversation. This system is built into Flask to achieve our aims of a Web Application to allow user interaction with the system.

To achieve the overall functionality of our framework we have build the following components using the modification of existing individual solutions to integrate and develop for our purposes:

1. Conversational System - Seq2Seq Encoder-Decoder LSTM neural network
2. Sign Language Detection - Tensorflow Object Detection
3. Sentiment Detection - Sklearn KNeighborClassifier (K-Nearest Neighbor Algorithm)
4. Web Application User Interface - Python Flask

#### Future Improvements could include:
1. Currently allows single user to interact with the system. Should we choose to make this accessible by multiple users, we can either generate a packaged/deployed application of the Flask app for individual distribution or to develop code for industrial deployments to say continuously run on server(s) enabling larger simultaneous interactions.
2. Implement account registration to allow users to be able to register and continue session.
3. Develop a Smartphone App to available for users to download.
4. Replace KNN for Sentiment Detection with a neural network for better classification for current or larger sentimental datasets
5. Expand Sign language dataset with more gestures and possibly multiple regional sign language datasets e.g. ASL/BSL/etc.
