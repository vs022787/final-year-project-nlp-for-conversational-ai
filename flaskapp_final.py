import cv2
import re
import os
import time
import speech_recognition as SpeechRecognition
from flask import Flask, Response, render_template, request, jsonify
from sign_language_detection import detect_signlang
from text_analysis import text_generation, get_sentiment, get_data_path
from frame_capture import FrameCapture

app=Flask(__name__)
app.secret_key = "thisismy_finalyearprojectworks"

detected_text_list = list()
detections_count = 15

def get_frames(captured_feed):
    """
    Performs analysis on the captured camera frames and returns
    frame, if detected returns with labeled detections, to then
    be displayed onto the web application feed.

    :param:captured_feed: FrameCapture class
    """
    
    detected_ct = 0
    temp_detected_list = []
    while captured_feed.video.isOpened():
        frame = captured_feed.capture_frame()
        detected_imgtxt, img_detections = detect_signlang(frame)
        _, enc_img = cv2.imencode('.jpg', cv2.resize(img_detections, (800, 600)))

        # print("Detected: {}".format(detected_imgtxt))
        if detected_imgtxt != "":
            temp_detected_list.append(detected_imgtxt)
            detected_ct+=1        
        if len(temp_detected_list) == detections_count:
            detected_imgtxt = max(set(temp_detected_list), key=temp_detected_list.count)
            detected_imgtxt = " ".join(re.findall('[A-Z][a-z]*', detected_imgtxt))
            print("\nDetected Sign Gesture: {}".format(detected_imgtxt))
            detected_ct = 0
            temp_detected_list = []
            detected_text_list.append(detected_imgtxt)
            time.sleep(3) # pauses video feed
        yield(b'--frame\r\n'
            b'Content-Type: image/jpeg\r\n\r\n' + enc_img.tobytes() + b'\r\n')


@app.route("/speech_text")
def speech_text():
    """
    Returns jsonified dictionary as response containing the 
    text representation of the captured speech from the microphone.
    """
    
    speech_capture = ""
    response_message = {}
    srecog = SpeechRecognition.Recognizer()
    try:
        with SpeechRecognition.Microphone() as source:
            print("CAPTURING SPEECH...")
            audio = srecog.listen(source, timeout=5)
            speech_capture = srecog.recognize_google(audio)
            response_message['speechText'] = speech_capture
            return jsonify(response_message)
    except Exception as e:
        print(e)
        response_message['speechText'] = ""
        return jsonify(response_message)

@app.route("/reset_sign")
def reset_sign():
    """
    Resets gesture detections list and returns a response
    """
    global detected_text_list
    detected_text_list = list()
    return jsonify({"status": "cleared"})

@app.route("/gestures_list")
def gestures_list():
    """
    Returns a jsonified response containing the list of 
    currently detected gestures
    """

    response_message = {"response": detected_text_list}
    return jsonify(response_message)
    

@app.post("/converse")
def converse():
    """
    Generates response to the input message provided along
    with the detected sentiment of both, the input and response
    """

    text_input = request.get_json().get("message")
    input_sent = get_sentiment(text_input)

    text_resp = text_generation(text_input)
    response_sent = get_sentiment(text_resp)

    response_message = {
        "message": text_input,
        "inputSentiment":input_sent,
        "response": text_resp,
        "respSentiment":response_sent}
    save_path = get_data_path()
    with open(os.path.join(save_path,"new_conversations.txt"), 'a') as f:
        save_text = "Human 1: {}\nhuman 2: {}\n".format(text_input, text_resp)
        f.write(save_text)
        f.close()
    
    return jsonify(response_message)


@app.route('/frames_feed')
def frames_feed():
    """
    Returns chunked responses of captured frames from camera feed 
    that have been processed by get_frames() to then display these 
    processed frames onto the web application.
    """
    return Response(get_frames(FrameCapture(device=0)),
    mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/', methods=["GET", "POST"])
def index():
    """
    Renders index HTML template
    """
    return render_template('index.html')


@app.route('/about')
def about():
    return render_template('about.html')


if __name__ == "__main__":
    app.run(debug=False)