import json
import re
import os
import pandas
import numpy
import pickle
from sklearn import preprocessing
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import metrics
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Model
from tensorflow.keras.layers import LSTM, Embedding, Input, Dense
from tensorflow.keras.utils import to_categorical

learn_new_convs = True
data_directory = os.path.join(os.getcwd(), "data")
newconv_limit = 100

def get_data_path():
    """
    Returns data folder path
    
    :return:data_directory: str"""
    return data_directory


def text_filtering(text):
    """
    Processes and filters the text by performing lowercasing
    and removing punctuations, abbreviations, and symbols to return
    filtered/formatted text

    :param:text: str
        Input string to be processed on
    :return:text: str
        Returns filtered/formatted text as string
    """

    if not isinstance(text, str):
        #raise TypeError("Not of type string")
        return None
    text = text.lower()
    text = re.sub(r"it's", "it is", text)
    text = re.sub(r"that's", "that is", text)
    text = re.sub(r"there's", "there is", text)
    text = re.sub(r"how's", "how is", text)
    text = re.sub(r"haven't", "have not", text)
    text = re.sub(r"what's", "what is", text)
    text = re.sub(r"i'm", "i am", text)
    text = re.sub(r"let's", "let us", text)
    text = re.sub(r"i'd", "i would", text)
    text = re.sub(r"don't","do not", text)
    text = re.sub(r"\'ve"," have", text)
    text = re.sub(r"[^\w\s]","", text)
    text = re.sub(r"won't","would not", text)
    text = re.sub(r"isn't","is not", text)
    text = re.sub(r"wasn't","was not", text)
    text = re.sub(r"\'ll"," will", text)
    text = re.sub(r"\'re"," are", text)
    text = re.sub(r"didn't","did not", text)
    text = re.sub(r"he's","he is", text)
    text = re.sub(r"she's","she is", text)
    text = re.sub(r"can't","can not", text)
    text = re.sub(r"doesn't","does not", text)
    text = re.sub(r"\'d"," would", text)
    text = re.sub(r"","", text)
    return text.strip()

def build_sentiment_model():
    """
    This function will build and train our sklearn KNeighborClassifier
    model on our own custom dataset that will be preprocessed and filtered.

    return: knclassif: obj, tfidfvect: obj
        Returns kneighborclassifier trained model and tf-idf object to 
        convert raw text into tf-idf matrix form
    """
    
    with open(os.path.join(
            data_directory, "custom_sentiment_dataset.txt"), "r") as f:
        raw = f.read().split("++")

    negative = raw[1].split("\n")[1:]
    positive = raw[2].split("\n")[1:]
    neutral = raw[3].split("\n")[1:]

    df = pandas.DataFrame(columns=['message', 'sentiment'])

    for sentiment in negative:
        filt = text_filtering(sentiment)
        if filt != "":
            df.loc[len(df.index)] = [filt, 'negative']

    for sentiment in positive:
        filt = text_filtering(sentiment)
        if filt != "":
            df.loc[len(df.index)] = [filt, 'positive']
            
    """for sentiment in neutral:
        filt = text_filtering(sentiment)
        if filt != "":
            df.loc[len(df.index)] = [filt, 'neutral']
    """
    df.fillna(0)

    messages = df['message']
    sentiments = df['sentiment']
    print(df.shape)

    tfidfvect = TfidfVectorizer(max_features = 100, lowercase=False)
    messages = tfidfvect.fit_transform(messages)

    msg_train, msg_test, sent_train, sent_test = train_test_split(
        messages,
        sentiments,
        test_size=0.1,
        random_state=0
        )

    # msg_train.shape

    knclassif = KNeighborsClassifier()
    knclassif.fit(msg_train, sent_train)

    get_pred = knclassif.predict(msg_test)

    # matrix confusion visualising
    print(metrics.confusion_matrix(sent_test, get_pred))

    # summary
    print(metrics.classification_report(sent_test, get_pred))

    # prediction accuracy
    print(metrics.accuracy_score(sent_test, get_pred))

    # test new data
    #input_text = "that's not funny"
    return tfidfvect, knclassif

def get_sentiment(input_text):
    """
    Returns the sentiment of an input text by being trained
    using sklearn KNeighborsClassifier model on our own created dataset that is
    preprocessed and filtered.

    :param:input_text: str
    The text we want to detect the sentiment

    :return:txt_sentiment: str
    Returns positive or negative string value as sentiment outcome by being fed 
    our through trained model
    """

    input_text = text_filtering(input_text)

    txt_vector = tfidfvect.transform([input_text])
    # txt_vector.shape
    txt_sentiment = knclassif.predict(txt_vector)
    print("Text: {} | Sentiment: {}\n".format(input_text, txt_sentiment[0]))
    return txt_sentiment[0]


def get_int_seq(text_list, vocabulary):
    """
    Tokenises text and generates to integer sequences using vocabulary.

    :param text: list
        Text list to peform sequences generation on
    :param vocabulary: dict
        Vocabulary of dataset text in dictionary

    :return integer_seq: numpy array
        Returns sequences representation
    """
    if not isinstance(text_list, list):
        #print("Not List, converting from {} to list...".format(str(type(text))))
        text_list = [text_list]

    integer_seq = []
    for text in text_list:
        temp_list = []
        if len(text_list) < 10:
            pass
            #print("DEBUG: {}".format(text))
        for word in text.split(" "):
            
            if word in vocabulary:
                temp_list.append(vocabulary[word])
            else:
                temp_list.append(vocabulary['<OUT>'])
        integer_seq.append(temp_list)
    return integer_seq

def load_saved_lstm(input_encode, input_decode, dense_output):
    """
    Loads saved layers weights and applies them to initialised model, 
    returning a trained model.

    :param: input_encode: obj
        Input encoder layer
    :param:input_decode: obj
        Input decoder layer
    :param:dense_output: obj
        Output Dense layer
    :return: obj, obj, obj
        Returns applied weighted input_encode, input_decode, dense_output objects
    """
    # "data_directory" contains the location path to the stored weights
    load_encode_embeddings = numpy.load(os.path.join(data_directory, 'encode_embedding_layer.npz'), allow_pickle=True)
    load_decode_embeddings = numpy.load(os.path.join(data_directory, 'decode_embedding_layer.npz'), allow_pickle=True)
    load_encode_lstm = numpy.load(os.path.join(data_directory, 'lstm_1.npz'), allow_pickle=True)
    load_decode_lstm = numpy.load(os.path.join(data_directory, 'lstm_2.npz'), allow_pickle=True)
    load_dense_layer = numpy.load(os.path.join(data_directory, 'dense.npz'), allow_pickle=True)
    
    # Initialize model
    model = Model([input_encode, input_decode], dense_output)
    model.layers[2].set_weights(load_encode_embeddings['arr_0'])
    model.layers[3].set_weights(load_decode_embeddings['arr_0'])
    model.layers[4].set_weights(load_encode_lstm['arr_0'])
    model.layers[5].set_weights(load_decode_lstm['arr_0'])
    model.layers[6].set_weights(load_dense_layer['arr_0'])
    return input_encode, input_decode, dense_output

def save_lstm_model(model):
    """
    Saves the provided model's weights to a physical location;
    the current directory's "data" folder

    :param:model: Model obj
        Model object we want to save weights of
    """

    for layer in model.layers:
        weights = layer.get_weights()
        if weights != []:
            npsave_path = os.path.join(
                data_directory, '{}.npz'.format(layer.name))
            if os.path.exists(npsave_path):
                open(npsave_path, 'w')
            numpy.savez(npsave_path, weights)

def build_lstm_model():
    """
    Builds LSTM model by pre-preprocessing dataset, constructing
    and training LSTM Neural Network using pre-processed dataset,
    returning trained encoder and decoder model, dense layer and 
    vocabulary.

    :return: vocabulary, encode_model, decode_model, dense_layer
    """

    # Pre-process dataset and generate vocabulary and padded sequences
    prepare_lstm_dataset()

    print("-----Training LSTM model-----")
    # ------------------ Encoder Decoder Model Building ------------------

    # sent_len, vocab_len, 
    input_layer_encode = Input(shape=(sent_len, ))
    input_layer_decode = Input(shape=(sent_len, ))
    with open(os.path.join(data_directory, "vocabulary.json"), 'r') as f:
        vocabulary = json.load(f)

    encode_embedding_layer = Embedding(len(vocabulary)+1, output_dim=30, 
                                input_length=sent_len, trainable=True, name="encode_embedding_layer")

    embed_input_encode = encode_embedding_layer(input_layer_encode)
    #LSTM(128, return_sequences=True, return_state=True)
    lstm_encode = LSTM(128, return_sequences=True, return_state=True, name="lstm_1")
    enc_out, enc_h, enc_c = lstm_encode(embed_input_encode)
    encoding_states = [enc_h, enc_c]

    decode_embedding_layer = Embedding(len(vocabulary)+1, output_dim=30, 
                                input_length=sent_len, trainable=True, name="decode_embedding_layer")
    embed_input_decode = decode_embedding_layer(input_layer_decode)
    lstm_decode = LSTM(128, return_sequences=True, return_state=True, name="lstm_2")
    dec_out, dec_h, dec_c = lstm_decode(embed_input_decode, initial_state=encoding_states)

    # Final Output Layer
    dense_layer = Dense(len(vocabulary), activation='softmax', name="dense")
    dense_output = dense_layer(dec_out)
    
    
    # Load and check new conversations
    with open(new_conversations_file, 'r') as f:
        new_convs = f.readlines()

    # if model path exists, load model AND
    # if file size not divisible, load model

    # if model doesn't exist, retrain model OR
    # if number of new conversations in file is greater 
    # than limit, retrain model
    print(os.path.exists(
                os.path.join(data_directory, 'dense.npz')))
    # if len(new_convs) % newconv_limit != 0
    if (len(new_convs) <= newconv_limit) and os.path.exists(
                os.path.join(data_directory, 'dense.npz')):
        print("Loading Saved model...")
        input_layer_encode, input_layer_decode, dense_output = load_saved_lstm(
            input_layer_encode, input_layer_decode, dense_output)
        print("Model Loaded")

    # if len(new_convs)) % newconv_limit == 0
    if (len(new_convs) >= newconv_limit or not os.path.exists(
                os.path.join(data_directory, 'dense.npz'))) and learn_new_convs:
        if len(new_convs) >= newconv_limit:
            # add new conversations to original dataset for retraining
            extend_dataset()
            
        # ------------------ MODEL TRAINING ------------------
        print("Training and Building LSTM model...")
        epochs = 600

        # generate padded sequences and vocabulary for updated dataset
        decoder_uttr, decoder_resp, step_decoder_resp = prepare_lstm_dataset()

        # load vocabulary
        with open(os.path.join(data_directory, "vocabulary.json"), 'r') as f:
            vocabulary = json.load(f)

        # BUILD LSTM NETWORK WITH INPUT, EMBEDDING ENCODER, EMBEDDING DECODER 
        # LSTM ENCODE, LSTM DECODE, AND DENSE LAYER
        input_layer_encode = Input(shape=(sent_len, ))
        input_layer_decode = Input(shape=(sent_len, ))
        encode_embedding_layer = Embedding(len(vocabulary)+1, output_dim=30, 
                                    input_length=sent_len, trainable=True, name="encode_embedding_layer")

        embed_input_encode = encode_embedding_layer(input_layer_encode)
        #LSTM(128, return_sequences=True, return_state=True)
        lstm_encode = LSTM(128, return_sequences=True, return_state=True, name="lstm_1")
        enc_out, enc_h, enc_c = lstm_encode(embed_input_encode)
        encoding_states = [enc_h, enc_c]

        decode_embedding_layer = Embedding(len(vocabulary)+1, output_dim=30, 
                                    input_length=sent_len, trainable=True, name="decode_embedding_layer")
        embed_input_decode = decode_embedding_layer(input_layer_decode)
        lstm_decode = LSTM(128, return_sequences=True, return_state=True, name="lstm_2")
        dec_out, dec_h, dec_c = lstm_decode(embed_input_decode, initial_state=encoding_states)
        
        # Final Output Layer
        dense_layer = Dense(len(vocabulary), activation='softmax', name="dense")
        dense_output = dense_layer(dec_out)
        # Build model for training and fitting
        model = Model([input_layer_encode, input_layer_decode], dense_output)
        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        model.fit([decoder_uttr, decoder_resp], step_decoder_resp, epochs=epochs)

        save_lstm_model(model)


    # ------------------ INFERENCE ------------------
    encode_model, decode_model = lstm_inference(
        input_layer_encode,
        input_layer_decode,
        lstm_decode,
        embed_input_decode,
        encoding_states
    )

    return vocabulary, encode_model, decode_model, dense_layer

def lstm_inference(input_layer_encode, input_layer_decode, lstm_decode, 
        embed_input_decode, encoding_states):
    """
    Returns encoding and decoding models for use in text generation
    where the provided arguments; input encode and decode, lstm and 
    embed decode with finally encoding states are used to generate
    the models.
    :param: input_layer_encode, input_layer_decode
    :param: lstm_decode
    :param: embed_input_decode
    :param: encoding_states
    
    :return: encode_model, decode_model
    """
    encode_model = Model([input_layer_encode], encoding_states)
    decoder_inp_h = Input(shape=(128, ))
    decoder_inp_c = Input(shape=(128, ))

    decoder_inp_states = [decoder_inp_h, decoder_inp_c]

    # passes previous states
    decoder_out, out_state_h, out_state_c = lstm_decode(embed_input_decode, 
                                                    initial_state=decoder_inp_states)

    decoder_states_out_list = [out_state_h, out_state_c]

    decode_model = Model([input_layer_decode] + decoder_inp_states,
                            [decoder_out] + decoder_states_out_list)
    return encode_model, decode_model

def prepare_lstm_dataset():
    punctuations = "[^\w\s]"
    punctuations2 = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''

    with open(model_dataset_file, 'r', encoding="utf-8") as f:
        file_dataset = f.readlines()

    num_lines = len(file_dataset)
    print(num_lines)
    #conversations = {}
    utterances = []
    responses = []
    # Here "file_dataset" contains a list of utterances and its following responses
    for each in file_dataset:
        text = ":".join(each.split(":")[1:]).strip()

        #if len(text.split()) < 15:
        if "human 1" in each.split(":")[0].lower():
            utterances.append(text)
        else:
            responses.append(text)
        #conversations[each.split(":")[0]] = [":".join(each.split(":")[1:])]

    filtered_utt = []
    filtered_resp = []

    for each, line in enumerate(utterances):
        # if length of query is less than "sent_len" words and its response is no greater than
        # "sent_len" words we keep these for training and remove long sentenced query/response
        try:
            if len(line.split(" ")) < sent_len and len(responses[each].split(" ")) < sent_len:
                filtered_utt.append(text_filtering(line))
                filtered_resp.append(text_filtering(responses[each]))
        except: # error will be thrown as last query doesn't have response 
            pass

    # Creating words count dictionary/vocabulary from dataset
    vocab_list = []
    wrd_vocab = {}
    for each in filtered_utt:
        vocab_list.extend(each.split())
        for w in each.split():
            if w not in wrd_vocab:
                wrd_vocab[w] = 1
            else:
                wrd_vocab[w] += 1
    for each in filtered_resp:
        vocab_list.extend(each.split())
        for w in each.split():
            if w not in wrd_vocab:
                wrd_vocab[w] = 1
            else:
                wrd_vocab[w] += 1

    vocabulary = {}
    wrd_ct = 0
    for each, freq in wrd_vocab.items():
        #if freq > 3
        vocabulary[each] = wrd_ct
        wrd_ct += 1

    vocabulary_len = len(vocabulary)

    # surround each text with special tokens
    for idx, txt in enumerate(filtered_resp):
        filtered_resp[idx] = '<SOS> {} <EOS>'.format(txt)

    # appending special tokens to end of vocabulary
    special_tokens = ['<PAD>' , '<SOS>', '<EOS>', '<OUT>']
    for each in special_tokens:
        vocabulary[each] = vocabulary_len
        vocabulary_len += 1

    print(">>>>>>>>>>>>>>\n")

    vocabulary['hi'] = vocabulary['<PAD>']
    vocabulary['<PAD>'] = 0

    vocab_file = os.path.join(data_directory, "vocabulary.json")
    with open(vocab_file, 'w') as f:
        json.dump(vocabulary, f)
    with open(vocab_file, 'r') as f:
        vocabulary = json.load(f)

    decoder_resp = []
    decoder_uttr = []
    list_len = len(filtered_utt)
    decoder_resp = get_int_seq(filtered_resp, vocabulary)
    decoder_uttr = get_int_seq(filtered_utt, vocabulary)

    print((decoder_uttr)[:3])
    #print((decoder_resp)[:3])

    decoder_uttr = pad_sequences(decoder_uttr, sent_len, padding='post', truncating='post')
    decoder_resp = pad_sequences(decoder_resp, sent_len, padding='post', truncating='post')
    #print(decoder_uttr)
    step_decoder_resp = []
    for decode in decoder_resp:
        step_decoder_resp.append(decode[1:])

    step_decoder_resp = to_categorical(
            pad_sequences(step_decoder_resp, sent_len, padding='post', truncating='post'),
        len(vocabulary))
    return decoder_uttr, decoder_resp, step_decoder_resp


def extend_dataset():
    """
    Extends model dataset when length of stored new messages
    in the file named 'new_conversations.txt' reaches limit.
    Wiping 'new_conversations.txt' file after extending dataset.
    """

    with open(new_conversations_file, 'r', encoding="utf-8") as f:
        new_conv = f.readlines()
    
    with open(model_dataset_file, 'a', encoding="utf-8") as file:
        for line in new_conv:
            file.write("\n"+str(line).strip())
    open(new_conversations_file, 'w').write("Human 1: Howdy\nHuman 2: Hey how's it going?\n")
    


model_dataset_file = os.path.join(data_directory, "human_chat.txt")
new_conversations_file = os.path.join(data_directory, "new_conversations.txt")

print("-----Building and Training sentiment detection model-----")
tfidfvect, knclassif = build_sentiment_model()

"""
decode_model = None
with open(new_conversations_file, 'r') as f:
    new_conv = f.readlines()
if len(new_conv) % 50 == 0:
    # extend model dataset and retrain model
    extend_dataset(),
"""

sent_len = 20
vocabulary, encode_model, decode_model, dense_layer = build_lstm_model()

def text_generation(text_input):
    """
    Generates response from provided text using trained model

    :param: text_input: str
    A string text we want to generate text on

    :return generated_text: str
    Returns generated text/response from model prediction

    """
    reversed_vocabulary = {num:wrd for wrd, num in vocabulary.items()}
    #print(reversed_vocabulary)
    text_input = text_filtering(text_input)
    # Performs padding on the integer sequences representation of input text
    text_input_seq = pad_sequences(
                get_int_seq(text_input, vocabulary),
                sent_len,
                padding='post'
    )

    user_states = encode_model.predict(text_input_seq)

    seq_build = numpy.zeros((1,1))
    seq_build[0,0] = vocabulary['<SOS>']
    COMPLETED = False
    generated_text = ""
    while COMPLETED == False:
        decoded_out, out_h, out_c = decode_model.predict(
            [seq_build] + user_states)
        decoded_input = dense_layer(decoded_out)

        # Get's highest probability score from result
        max_index = numpy.argmax(decoded_input[0, -1, :])

        #convert integer index back into word using inverse vocabulary to be outputted
        index_text = "{} ".format(reversed_vocabulary[max_index])
        if index_text != "{} ".format('<EOS>'):
            generated_text += index_text
        if index_text == '<EOS> ' or len(generated_text.split()) > sent_len:
            COMPLETED=True
        
        # pass into next iteration
        seq_build = numpy.zeros((1,1))
        seq_build[0,0] = max_index
        user_states = [out_h, out_c]
    return generated_text

if __name__ == '__main__':
    while True:
        (get_sentiment(input("Enter Text: ")))