class ConversationBox {
    constructor() {
        // Selects interested HTML tags
        this.args = {
            blockContainer: document.querySelector('.chatblock'),
            sendMessageBtn: document.querySelector('.submit_message')
        }
    }
    show() {
        const {blockContainer, sendMessageBtn} = this.args;
        // Adds event listener on submit message button and triggers function
        sendMessageBtn.addEventListener('click', () => this.onSubmitBtn(blockContainer))
        // Adds event listener to key input; Enter keypress
        const eventNode = blockContainer.querySelector('input');
        eventNode.addEventListener('keyup', ({key}) => {
            if (key === "Enter") {
                //triggers function to generate response
                this.onSubmitBtn(blockContainer)
            }
        })
    }

    onSubmitBtn(boxArea) {
        // Extracts value from HTML tag
        var textField = boxArea.querySelector('input');
        let textInput = textField.value;
        if (textInput === "") {
            return;
        }
        // Resets/Clears HTML tag value
        textField.value = ''
        
        // Sends POST request with extracted HTML tag's value to
        // receive response
        fetch($SCRIPT_ROOT + '/converse', {
            method: 'POST',
            body: JSON.stringify({message: textInput}),
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
        })
        .then(postResp => postResp.json())
        .then(postResp => {
            // Display 
            var textboxarea = boxArea.querySelector('textarea');
            var myMessage = "Me: "+postResp.message+
                " | Sentiment: "+postResp.inputSentiment+"\n"
            console.log(myMessage)
            // Appends user messages in conversation history on UI
            textboxarea.innerHTML += myMessage
            
            // Appends generated response message to textarea HTML tag
            var respMessage = "myConversingAI: "+postResp.response+
                " | Sentiment: "+postResp.respSentiment+"\n"
            console.log(respMessage)
            textboxarea.innerHTML += respMessage
        })
        .catch((err) => { // Returns any caught errors to console
            console.error('Error: ', err);
            textField.value = ''
        });
    }
}

const boxArea = new ConversationBox();
boxArea.show()