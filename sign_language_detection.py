import os
import numpy
import tensorflow as tf
import cv2
from object_detection.utils import label_map_util, config_util
from object_detection.utils import visualization_utils as viz_utils
from object_detection.builders import model_builder
from matplotlib import pyplot as plt


IMPORT_MODEL_NAME = 'ssd_mobnet320x320_model' 
#PRETRAINED_MODEL_NAME = 'ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8'
#PRETRAINED_MODEL_URL = 'http://download.tensorflow.org/models/object_detection/tf2/20200711/ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8.tar.gz'

total_checkpoints = "6"
learning_epochs = 15000
model_directory = os.path.join(os.path.join('Tensflw', 'ws'), 'models')
pipeline_path = os.path.join(model_directory, IMPORT_MODEL_NAME, 'pipeline.config')
checkpoint_path = os.path.join(os.path.join(model_directory,IMPORT_MODEL_NAME),
            'ckpt-{}'.format(total_checkpoints))
labelmap_path = os.path.join(os.path.join('Tensflw', 'ws','annotations'), 'label_map.pbtxt')

#LBLIMG_path = os.path.join("Tensflw", "labelimg")
#TF_RECORD_SCRIPT_NAME = 'generate_tfrecord.py'
@tf.function
def model_detect(image):
    """
    Performs prediction/gesture detection on tensor image type

    :param image: tensor image
    :return detections: dict
        Returns dictionary
    """

    image, shapes = detection_model.preprocess(image)
    prediction_dict = detection_model.predict(image, shapes)
    detections = detection_model.postprocess(prediction_dict, shapes)
    return detections

def model_training():
    tranining_script = os.path.join('tensflw','models', 'research', 'object_detection', 'model_main_tf2.py')
    command = "python {} --model_dir={} --pipeline_config_path={} --num_train_steps={}".format(
    tranining_script, checkpoint_path, pipeline_path, str(learning_epochs)
    )
    print(command)
    #!{command}

def detect_signlang(img):
    """
    Performs gesture detections on the provided image and returns detected frame and 
    text.
    
    :param img: str/jpg
        Frame/Image path provided to perform detection
    :return detected_imgtxt: str, img_with_detections: numpy image
    """
    # Get labels of gestures used
    category_idx = label_map_util.create_category_index_from_labelmap(labelmap_path)
    if isinstance(img, str):
        img = cv2.imread(img)
    image_np = numpy.array(img)
 
    # Converting from numpy to tensor and performing detections
    input_tensor = tf.convert_to_tensor(numpy.expand_dims(image_np, 0), dtype=tf.float32)
    detections = model_detect(input_tensor)
    num_detections = int(detections.pop('num_detections'))
    #detections = {key: value[0, :num_detections].numpy()
    #            for key, value in detections.items()}

    tmp_dict = {}
    for key, value in detections.items():
        tmp_dict[key] = value[0, :num_detections].numpy()
    detections = tmp_dict

    detections['num_detections'] = num_detections
    detections['detection_classes'] = detections['detection_classes'].astype(numpy.int64)

    label_id_offset = 1
    img_with_detections = image_np.copy()

    _, detected_imgtxt = viz_utils.visualize_boxes_and_labels_on_image_array(
                img_with_detections,
                detections['detection_boxes'],
                detections['detection_classes']+label_id_offset,
                detections['detection_scores'],
                category_idx,
                use_normalized_coordinates=True,
                max_boxes_to_draw=5,
                min_score_thresh=.8,
                agnostic_mode=False)

    #print("Detected Gesture: {}".format(detected_imgtxt))

    #plt.imshow(cv2.cvtColor(img_with_detections, cv2.COLOR_BGR2RGB))
    #plt.show()
    
    return detected_imgtxt, img_with_detections

print("-----Building and Training sentiment detection model-----")

# Load pipeline configurations and restore saved trained detection model
configs = config_util.get_configs_from_pipeline_file(pipeline_path)
detection_model = model_builder.build(model_config=configs['model'], is_training=False)
# Restore checkpoint
ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)
ckpt.restore(checkpoint_path).expect_partial()


